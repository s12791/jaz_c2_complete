package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Access;
import domain.User;

@WebServlet("/data")
public class DataServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		ServletContext app = getServletContext();
			
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		
		User u = new User();
		u.setUsername(username);
		u.setPassword(password);
		u.setEmail(email);
		u.setAccess(Access.zwykly);
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><h2>Zarejestrowano uzytkownika</h2>" +
				"<p>Login: " + username + "<br />" +
				"<p>Haslo: " + password + "<br />" +
				"<p>Email: " + email + "<br />" +
				"<p>Typ konta: zwykly <br />" +
				"<br><a href='login'>Zaloguj sie!</a>"+
				"</body></html>");
		out.close();
		
		((HashMap<String,User>) app.getAttribute("osoby")).put(username,u);
	}

}