package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Access;
import domain.User;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	public void init() throws ServletException {
		  
        HashMap<String,User> osoby = new HashMap<String,User>();
        
        User u = new User();
		u.setUsername("admin");
		u.setPassword("admin1234");
		u.setEmail("");
		u.setAccess(Access.admin);
        osoby.put("admin", u);
		
		ServletContext context = getServletContext();
		context.setAttribute("osoby", osoby);
	}
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		ServletContext context = getServletContext();
		context.setAttribute("logged", false);
		
		
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><h2>Zaloguj sie!</h2>"
				+ "<form action='main'>"
				+ "<table>"
				+ "<tr>"
					+ "<td>Name:</td>"
					+ "<td><input type='text' name='username'></td>"
				+ "</tr>"
				+ "<tr>"
					+ "<td>Password:</td>"
					+ "<td><input type='password' name='password'></td>"
				+ "</tr>"
				+ "</table>"
				+ "<td><input type='submit' value='Login'>"
			+ "</form>"
			+ "<p>Nie masz konta?  <a href='form'>Zarejestruj sie!</a></p>"
			+"</body></html>");
		out.close();
	}

}


