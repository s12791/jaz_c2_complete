package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Access;
import domain.User;


@WebServlet("/accessform")
public class AccessFormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		ServletContext app = getServletContext();	
		
		String username = request.getParameter("name");
		String type = request.getParameter("type").toString();
		Access access = Access.zwykly;
		
		if(type.equals("zwykly"))access=Access.zwykly;
		else 
			if(type.equalsIgnoreCase("premium"))access=Access.premium;
			else
				if(type.equals("admin"))access=Access.admin;
			
		((HashMap<String,User>) app.getAttribute("osoby")).get(username).setAccess(access);
		
		
		PrintWriter out = response.getWriter();
		out.println("<html><body>Zmieniono uprawnienia uzytkownika: "+username+" na: "+type+
				"<br><a href='Accesform.jsp'>Powrot do panelu administracyjnego</a>"+
			"</body></html>");
	
		
		out.close();
	}

}