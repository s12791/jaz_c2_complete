package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/main")
public class MainServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><h2>Strona glowna</h2>" +
				"Zalogowano jako "+request.getSession().getAttribute("username")+"!<br>"+
				"<br><a href='profil'>Wyswietl profil</a>"+
				"<br><a href='premium.jsp'>Strona Premium</a>"+
				"<br><a href='Accesform.jsp'>Panel administacyjny</a>"+
				"<br><br><a href='index.jsp'>Wyloguj</a>"+
			"</body></html>");
		out.close();
	}

}