package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.User;

@WebServlet("/showall")
public class ShowAllServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ServletContext app = getServletContext();
		response.setContentType("text/html");
		HashMap<String,User> osoby = (HashMap<String,User>) app.getAttribute("osoby");
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><h2>Lista uzytkownikow</h2>"
				+ "username, password, email, access<br><br>");
		if (osoby!=null){
			for (User u :osoby.values())
				out.print(u.getUsername()+", "+u.getPassword()+", "+u.getEmail()+" ("+u.getAccess().toString()+")<br>");
		}
				
		out.print("<br><a href='Accesform.jsp'>Powrot do panelu administracyjnego</a>"+
			"</body></html>");
		out.close();
	}

}