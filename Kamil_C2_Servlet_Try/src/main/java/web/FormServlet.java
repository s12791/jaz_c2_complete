package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
	
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Formularz rejestracyjny</h2>"
				+ "<form action='data'>" 
				+ "<table>"
					+ "<tr>"
						+ "<td>Imie:</td>"
						+ "<td><input type='text' name='username' required/></td>"
					+ "</tr>"
					+ "<tr>"
						+ "<td>Haslo:</td> "
						+ "<td><input type='password' name='password' required/></td>"
					+ "</tr>"
						+ "<td>Potwierdz haslo:</td> "
						+ "<td><input type='password' name='check' required/></td>"
					+ "<tr>"
						+ "<td>Email:</td>"
						+ "<td><input type='email' name='email' required/></td>"
					+ "</tr>"
				+ "</table>"
				+ "<br><input type='submit' value=' OK ' />"
				+ "</form>"
				+ "<a href='login'>Powrot</a>"
			+ "</body></html>");
		out.close();
	}

}