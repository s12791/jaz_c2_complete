package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.User;

@WebServlet("/profil")
public class ProfilServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		ServletContext app = getServletContext();
		
		String username=(String) request.getSession().getAttribute("username");
		HashMap<String, User> osoby = (HashMap<String,User>) app.getAttribute("osoby");
		
		out.print("<h2>Witaj "+username+". Oto twoj profil.</h2>");
		out.print(
				"<br><a href='premium.jsp'>Strona Premium</a>"+
				"<br><a href='main'>Powrot</a>"+
				"<br><a href='index.jsp'>Wyloguj</a>"+
			"</body></html>");
		out.close();
			}
		}