package filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Access;
import domain.User;

@WebFilter("/premium.jsp")
public class PremiumFilter implements Filter{

	FilterConfig filterConfig = null;

	private ServletContext context;

	public void init(FilterConfig config) throws ServletException {
		this.context= config.getServletContext();
	}
	
	
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		
		PrintWriter out = resp.getWriter();
		
		HashMap<String,User> osoby = (HashMap<String,User>) context.getAttribute("osoby");
	
		
		if (osoby.get(req.getSession().getAttribute("username")).getAccess()!=Access.zwykly)
		{
			chain.doFilter(req, resp);
		}
		else 
		{
			resp.sendRedirect("profil");
		}
		
		out.close();
		
	}
	public void destroy() {}


}